/*
 * 本代码是用来进行串口解析默认为Arduino平台 可移植至其他平台
 * 本代码中 用Serial.print作为串口输出 若非Arduino平台，更换成相应UART函数输出即可
 */

#ifndef __CMD__
#define __CMD__
/*系统库*/
#include <Arduino.h>
   
/*用户库调用*/

#define CMD_LIST_SIZE 100     //一百条cmd上限
#define CMD_LENGTH 20         //单条cmd最大名称长度
#define CMD_MESSAGE_LENGTH 50 //单条cmd最大消息长度

typedef int (*cmd_handler)(void);

typedef struct
{
    char cmd_name[CMD_LENGTH];            //名称
    char cmd_message[CMD_MESSAGE_LENGTH]; //描述信息
    int node_hash;                        //哈希节点
    cmd_handler hander;                   //函数指针
} cmdNode;

extern cmdNode cmd_list[CMD_LIST_SIZE];

namespace CMD
{
    void init(void);                      //初始化 注册命令行
    void parsing(char name[CMD_LENGTH]); //查找命令行
    void Register(const char cmd_name[CMD_LENGTH],const char cmd_message[CMD_MESSAGE_LENGTH],cmd_handler func_name,int hash_node);
    void getCMD(void);
}

#endif