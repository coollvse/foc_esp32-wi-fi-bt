#ifndef __BAT__
#define __BAT__

#include <Arduino.h>
#include "Config.h"
namespace BAT{
    // extern float vol;

    struct sBatUnit {
        float Vol;
        char* info;
    };

    void Init(void);
    void getVol(void);
}

#endif